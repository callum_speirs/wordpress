#!/bin/sh

# Run wp-cli as www-data from /srv/www/
PATH=$(dirname $0):${PATH}
export WP_CLI_FINDER_SCRIPT="$(command -v wp-core) --path=/srv/www/"
command=${WP_CLI_FINDER_SCRIPT}
for argument in "$@"; do
  command="${command} \"${argument}\""
done
su -s /bin/sh -c "${command}" www-data
